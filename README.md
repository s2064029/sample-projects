## What you should do
- create your own virtual environment
    - ``source /home/tang2/toolchain/toolchain.rc``
    - ``python -m venv my_venv3.8``
    - ``. my_venv3.8/bin/activate``
    - install python package via ``pip install``
- (optional) 
    - create ``.bash_profile``
        ```
        if [ -f ~/.bashrc ]; then
            . ~/.bashrc
        fi
        ```
    - add ``. /home/htang2/toolchain/toolchain.rc`` in ``.bashrc``
## Basics
- A ``node`` is a machine
- Each ``partition`` contains a set of ``nodes``
    - Different nodes have different time limit

## Useful Commands
- ``sinfo`` (check status of each partition)
- ``squeue`` (see who's running jobs / check job status)
    - ``squeue -u s21xxxxx``
    - ``squeue -o "%u %j %o"``
    - ``man squeue``
- ``sbatch`` (to run jobs)
    - ``sbatch -p Teach-Standard ./run-training.sh``
    - ``sbatch -p Teach-Standard --gres=gpu:1 ./run-training.sh``
- ``srun`` (to debug)
    - ``srun -p Teach_Interactive --pty bash``
    - ``srun -p Teach_Interactive --gres=gpu:1 --pty bash``
- ``scancel`` (cancel specific job base on jobs ids)
- ``nvidia-smi`` (get GPU status)
- ``hostname`` (get current host name)

## How to submit simple jobs (job1.sh)
- change configuration in ``conf/exp1.yaml``
- ``sbatch job1.sh``

## How to train jobs with longer required time (job2.sh)
- ``for i in 1 2 3 4 5; do sbatch -p Teach-Standard -d singleton -J LSTM-job ./run-training.sh $i; done``
- ``for i in 1 2 3 4 5; do sbatch -p Teach-Standard --wait ./run-training.sh $i; done``

## How to submit multiple jobs with single script (job3.sh)
- ``sbatch -p Teach-Standard --array=1-3 ./job3.sh job3-jobs``

## Requirements
- ``pip install hydra-core --upgrade``
- ``pip install torch torchvision torchaudio``

## Usage

### Submit jobs
| job id | Description | Usage |
| --- | ----------- | ----------|
| job1 | Submit a single job | ``sbatch job1.sh`` |
| job2 | Submit concatenated jobs to train a longer job  | ``./job2.sh`` |
| job3 | Submit array jobs (multiple jobs) | ``sbatch job3.sh job3-list`` |

### Configure hyperparames
- ``train.yaml`` is for redirection only
- Modify ``conf/exp1.yaml`` or ``conf/exp2.yaml``
- Choose config file for training
    ```
    python train.py conf=exp2
    ```
- Modify configuration on command line 
    ```
    python train.py conf=exp2 train.batch=16 train.lr=0.1
    ```

## Warning
- Don't use ``SL7``: they are old

## Reference for fun features
- https://hydra.cc
- https://github.com/ashleve/lightning-hydra-template
- https://omegaconf.readthedocs.io/en/latest/usage.html#access-and-manipulation