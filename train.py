# created on Mar 10, 2022

import hydra, yaml, functools, os, logging, argparse
from omegaconf import DictConfig, OmegaConf

import torch
import torch.nn as nn

torch.manual_seed(0)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
log = logging.getLogger(__name__)

class join(yaml.YAMLObject):
    yaml_loader = yaml.SafeLoader
    yaml_tag = '!join'
    @classmethod
    def from_yaml(cls, loader, node):
        return functools.reduce(lambda a, b: a.value + b.value, node.value)

class classifier(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(classifier, self).__init__()
        self.ln_layer = nn.Linear(in_ch, out_ch)
        
    def forward(self, x):
        return self.ln_layer(x)

@hydra.main(config_path=".", config_name="train")
def main(cfg: DictConfig):
    # check you load the right configuration
    print(OmegaConf.to_yaml(cfg))

    # create folder to save model checkpoint
    os.makedirs('ckpt', exist_ok=True)

    # get configuration
    bs = cfg.train.batch
    ep = cfg.train.epoch
    lr = cfg.train.lr

    # parse configuration
    # cfg.ckpt.epoch = int(cfg.ckpt.epoch)

    # load 40-dim fbanks
    fbanks = torch.randn((bs, 1000, 40), dtype=torch.float).to(device)

    # load target phones, 42 phones for wsj corpus
    phones = torch.randint(low=0, high=41, size=(bs, 1000)).to(device)

    # init model
    model = classifier(40, 42).to(device)
    optim = torch.optim.Adam(model.parameters(), lr=lr)

    # load model from checkpoint
    if cfg.ckpt.epoch > 0:
        model_path = os.path.join(cfg.ckpt.path, f'model_{cfg.ckpt.epoch}.ckpt')
        optim_path = os.path.join(cfg.ckpt.path, f'optim_{cfg.ckpt.epoch}.ckpt')
        model.load_state_dict(torch.load(model_path, map_location=device), strict=False)
        optim.load_state_dict(torch.load(optim_path, map_location=device))
        st_epoch = cfg.ckpt.epoch + 1
        ed_epoch = cfg.ckpt.epoch + ep + 1
    else:
        torch.save(model.state_dict(), os.path.join('ckpt', 'model_0.ckpt'))
        st_epoch = 1
        ed_epoch = ep + 1

    # define loss
    CrossEntropyLoss = nn.CrossEntropyLoss(reduction='mean')

    for e in range(st_epoch, ed_epoch):
        # clean gradient
        optim.zero_grad()

        # forward pass
        pred = model(fbanks)

        # compute loss
        loss = CrossEntropyLoss(pred.view(-1, 42), phones.view(-1))

        # compute gradient
        loss.backward()

        # update model params
        optim.step()

        log.info(f'training iter: {e:2d}, loss:{loss.item():.3f}')

    torch.save(model.state_dict(), os.path.join('ckpt', f'model_{e}.ckpt'))
    torch.save(optim.state_dict(), os.path.join('ckpt', f'optim_{e}.ckpt'))


if __name__ == '__main__':
    main()