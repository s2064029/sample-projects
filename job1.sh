#!/bin/bash

#SBATCH -p Teach-Standard
#SBATCH --exclude=damnii08,damnii09
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --output=slurm/%J.out
#SBATCH --array=0

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000

hostname
. ~/toolchain/toolchain.rc
. ~/my_venv3.8/bin/activate

# python train.py
python train.py conf=exp1 train.batch=16 train.lr=0.1
