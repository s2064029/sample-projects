#!/bin/bash

# hostname
# . ~/toolchain/toolchain.rc
# . ~/my_venv3.8/bin/activate

for i in 0 10 20 30 40 50
do 
    sbatch -d singleton -J LSTM-job ./job2-single-job.sh $i
done