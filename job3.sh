#!/bin/bash

#SBATCH -p Teach-Standard
#SBATCH --exclude=damnii08,damnii09
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --output=slurm/%J.out
#SBATCH --array=1-3

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000

hostname
. ~/toolchain/toolchain.rc
. ~/my_venv3.8/bin/activate

cmd=$(sed -n "${SLURM_ARRAY_TASK_ID}p" $1)
eval "$cmd" > slurm/job3-${SLURM_ARRAY_TASK_ID}.out
